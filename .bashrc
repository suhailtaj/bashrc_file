# Node Version Manager
if [ -d ~/.nvm ]; then
	export NVM_DIR=~/.nvm
	[ -s "$NVM_DIR/nvm.sh" ] && source "$NVM_DIR/nvm.sh"  # This loads nvm
	nvm use stable
fi

# Ruby Version Manager
if [ -d ~/.rvm ]; then
	[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
fi


# Docker Version Manager
if [ -d ~/.dvm ]; then
	source ~/.dvm/dvm.sh
	dvm use default
fi

# VS Code Alias Function
code () { VSCODE_CWD="$PWD" open -n -b "com.microsoft.VSCode" --args $* ;}


# Docker Alias
alias dps='docker ps -a'
alias d='docker $@'
alias dc='docker-compose $@'
alias di='docker images'
function din(){
	docker exec -i -t $1 /bin/sh
}

# Custom alias
alias c='clear'
alias ubuntu='ssh ubuntu@nsnui.sensity.com'

# open projects - Speed-dial
function project (){
	usage() {
		usage=$(cat <<-EOF

		Usage: project [Speed-dial number] [code edditor]

		Speed-dial options:
   		1 => Project Sensity Fork Farallones
   		2 => Project Sensity NSN_functional_testing
   		3 => Project Sensity Farallones-Docker-Orchestration

		EOF)
		
		echo "$usage"
	}

	# Main program
	[ $# -eq 0 ] && usage
	while [ -n "$1" ]; do
		case "$1" in
			1)
				cd  ~/source/projects/Sensity/fork-Farallones/ui_apps/netsense3/
				${2:-code} .
				break
				;;
			2)
				cd ~/source/projects/Sensity/NSN_functional_testing/
				${2:-code} .
				break
				;;
			3)
				cd ~/source/projects/Sensity/Farallones-Docker-Orchestration/
				${2:-code} .
				break
				;;
			-h)
                usage
                break
                ;;
			*)
                usage
                break
                ;;
		esac
	done
}


# Fork sync
function forkSync(){
	usage() {
		usage=$(cat <<-EOF

		Usage: forkSync [ Branch_Name ]

		EOF)

		echo "$usage"
	}

	main(){
		git fetch upstream
		git checkout $1
		git merge upstream/$1
	}

	[ $# -eq 0 ] && usage

	while [ -n "$1" ]; do
		main
	done	
	
}
